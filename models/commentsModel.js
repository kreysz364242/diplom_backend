const {
    Sequelize,
    sequelize
} = require('../lib/sequlize')
const Places = require('./PlaceModel')
const Model = Sequelize.Model
const Users = require('./UserModel')
class Comments extends Model {}
Comments.init({
    commentText: {
        type: Sequelize.STRING(1024),
        allowNull: false,
    },
    rating: {
        type: Sequelize.DOUBLE,
        allowNull: false
    }
}, {
    timestamps: false,
    sequelize,
    modelName: 'Comment'
});
Places.hasMany(Comments)
Users.hasMany(Comments)
Comments.belongsTo(Users, {
    foreignKey: 'UserId'
})
module.exports = Comments