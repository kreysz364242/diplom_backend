'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {

      await queryInterface.addColumn(
          'Comments',
          'rating', {
              type: Sequelize.DOUBLE,
              allowNull: false
          }, { transaction }
      );
      await transaction.commit();
    } catch (err) {
        await transaction.rollback();
        throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Comments', 'rating', { transaction });
      await transaction.commit();
    } catch (err) {
        await transaction.rollback();
        throw err;
    }
  }
};
