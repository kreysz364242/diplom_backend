const Places = require('../models/PlaceModel')
const Citys = require('../models/cityModel')
const Images = require('../models/imagesModel')
const chalk = require('chalk')
const cherio = require('cherio')
const {
    arrayFromLength,
    getCity
} = require('./helpers/common')
const {
    getPageContent
} = require('./helpers/puppeteer')
const getImage = require('./helpers/getImage')


// const SITE = "https://tonkosti.ru/Достопримечательности_Нижнего_Новгорода?page="
// const numbersOfPage = 4
// const city = 'Нижний Новгород'



async function scrapper(SITE, numbersOfPage, city) {
    try {
        for (const page of arrayFromLength(numbersOfPage)) {
            let url = ""
            if (page === 1)
                url = `${SITE}`
            else url = `${SITE}?page=${page}#ttl`
            const pageContent = await getPageContent(url)
            const $ = cherio.load(pageContent)
            let CityId

            for (const places of Object.values($('.places-list__item-rc'))) {
                let curLocation = $(places).children('.places-list__add-info').children('.places-list__address').text()
                if (curLocation.split(',')[0] === city) {
                    let images = await getImage('https://tonkosti.ru' + $(places).children('h3').children('a').attr('href'))
                    if (!images) {
                        images = []
                        images.push($(places).children('.places-list__item-img').children('img').attr('src'))
                    }

                    let locationName = $(places).children('h3').children('a').text()
                    let locationDescription = $(places).children('.places-list__text').text() + ($(places).children('.places-list__text').children('a').text() || "")
                    try {
                        let location = curLocation
                        let city = getCity(location)
                        if (!CityId) {
                            const cityDB = await Citys.findOne({
                                where: {
                                    cityName: city
                                }
                            })
                            if (cityDB) CityId = cityDB.id
                            else {
                                const newCityDB = await Citys.create({
                                    cityName: city
                                })
                                CityId = newCityDB.id
                            }
                        }

                        const place = await Places.findOne({
                            where: {
                                location: location
                            }
                        })
                        if (place) throw new Error('place already exist')
                        const newPlace = await Places.create({
                            locationName,
                            locationDescription,
                            location,
                            CityId
                        })
                        images = images.map(image => ({
                            image,
                            PlaceId: newPlace.id
                        }))
                        await Images.bulkCreate(images)
                    } catch (error) {
                        console.log(chalk.red(error.message))
                    }
                }
            }
        }
    } catch (error) {
        console.log(chalk.red('Error'))
        console.log(error)
    }
    console.log('end')
}

module.exports = scrapper