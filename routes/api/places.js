const router = require('express').Router();
const Places = require('../../models/PlaceModel')
const City = require('../../models/cityModel')
const Images = require('../../models/imagesModel')
const Users = require('../../models/UserModel')
const Comments = require('../../models/commentsModel')
const checkUser = require('../../middleware/checkUser')
const {
    Sequelize
} = require('../../lib/sequlize')
const createError = require('../../lib/createError')
const passport = require('passport')

const checkJwt = require('../../lib/decodeTokens')

router.get('/getPlaces', async (req, resp, next) => {
    let cityName = req.query.cityName
    try {
        if (!cityName) next(createError('cityName not exist', 402))
        const city = await City.findOne({
            where: {
                cityName: cityName
            }
        })
        if (!city) return next(createError('city not found', 402))
        const places = await Places.findAll({
            where: {
                CityId: city.id
            },
            include: [{
                attributes: ['image'],
                model: Images
            }, {
                attributes: ['commentText'],
                model: Comments
            }]
        })
        const dataPlaces = places.map(place => {
            const placeData = {
                ...place.dataValues
            }
            placeData.Images = placeData.Images.map(image => image.image)
            placeData.CommentsLength = placeData.Comments.length
            if (placeData.rewiewCount && placeData.rating) {
                if (placeData.rewiewCount !== 0 && placeData.rating !== 0) {
                    placeData.placeRating = placeData.rating / placeData.rewiewCount
                } else {
                    placeData.placeRating = 0
                }
            } else {
                placeData.placeRating = null
            }

            return placeData
        })
        resp.status(200).json(dataPlaces)
    } catch {
        return next(createError('db error', 500))
    }

})

router.post('/addComment', checkJwt, checkUser, async ({
    nickname,
    sub
}, req, resp, next) => {
    const {
        body: {
            data
        }
    } = req;
    try {
        const userDb = await Users.findOne({
            where: {
                auth0Id: sub
            }
        })
        if (!userDb) return next(createError('user not exist', 402))
        await Comments.create({
            commentText: data.commentText,
            PlaceId: data.id,
            rating: data.rating || 0,
            UserId: userDb.id
        })
        const place = await Places.findOne({
            where: {
                id: data.id
            }
        })
        if (place) {
            if (!place.rating)
                await Places.update({
                    rating: data.rating,
                    rewiewCount: 1
                }, {
                    where: {
                        id: data.id
                    }
                })
            else
                await Places.update({
                    rating: place.rating + data.rating,
                    rewiewCount: ++place.rewiewCount
                }, {
                    where: {
                        id: data.id
                    }
                })
        }
        resp.status(200).json('ok')
    } catch (error) {
        console.log(error)
        return next(createError('db error', 500))
    }
})

router.get('/getCities', async (req, resp, next) => {
    try {
        const cities = await City.findAll()
        resp.status(200).json(cities)
    } catch {
        return next(createError('db error', 500))
    }
})

router.get('/getRating', async (req, resp, next) => {
    try {
        let PlaceId = req.query.PlaceId

        const place = await Places.findOne({
            where: {
                id: PlaceId
            }
        })
        if (place.rewiewCount && place.rating) {
            if (place.rewiewCount !== 0 && place.rating !== 0) {
                place.placeRating = place.rating / place.rewiewCount
            } else {
                place.placeRating = 0
            }
        } else {
            place.placeRating = null
        }
        resp.status(200).json(place.placeRating)
    } catch (err) {
        console.error(`Error (${err})`)
    }
})

router.get('/getPlaceComments', async (req, resp, next) => {
    try {
        let PlaceId = req.query.PlaceId
        let nickName = req.query.nickName || ''
        const comments = await Comments.findAll({
            where: {
                PlaceId: PlaceId
            },
            include: [{
                model: Users
            }]
        })

        const commentsVal = []
        let alreadyRewiew = false

        for (let comment of comments) {
            if (comment.User) {
                const commentValues = comment.dataValues
                commentValues['nickName'] = comment.User.dataValues.nickName
                commentValues['picture'] = comment.User.dataValues.picture
                if (nickName === comment.User.dataValues.nickName)
                    alreadyRewiew = true
                commentsVal.push(commentValues)
            } else {
                await Comments.destroy({
                    where: {
                        id: comment.dataValues.id
                    }
                })
            }
        }

        resp.status(200).json({
            commentsVal,
            alreadyRewiew
        })
    } catch (err) {
        console.log(err)
        return next(createError('db error', 500))
    }
})

module.exports = router