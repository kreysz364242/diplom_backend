const {
    Sequelize,
    sequelize
} = require('../lib/sequlize')
const crypto = require('crypto');

const Model = Sequelize.Model
class Users extends Model {

    validatePassword(password) {
        const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 256, 'sha512').toString('hex');
        return this.hash === hash;
    }
    setPassword(password) {
        this.salt = crypto.randomBytes(16).toString('hex')
        this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 256, 'sha512').toString('hex')
    }
}
Users.init({
    nickName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    auth0Id: {
        type: Sequelize.STRING,
        allowNull: false
    },
    picture: {
        type: Sequelize.STRING(512),
        allowNull: true
    },
}, {
    timestamps: false,
    sequelize,
    modelName: 'User'
});


module.exports = Users
// User.sync({ force: true }).then(() => {
//   const a = User.build({ email: 'mail@fslfd.ru'})
//   a.setPassword('sss')
//   console.log(a.getSaltHashAndEmail())
//   return a.save()
// })
//   .then(employee => {
//     console.log("мама жива"); // John Doe (SENIOR ENGINEER) // SENIOR ENGINEER
//   }).catch((error)=> console.log(error))