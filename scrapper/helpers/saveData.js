
const path = require('path')
const fs = require('fs')
const chalk = require('chalk')

const pathFile = "taganrog"

module.exports = async function saveData(data) {
  const fileName = `${pathFile}.json`;
  const savePath = path.join(__dirname, '..', 'data', fileName);

  return new Promise((resolve, reject) => {
    fs.writeFile(savePath, JSON.stringify(data, null, 4), err => {
      if (err) {
        return reject(err);
      }

      console.log(chalk.blue('File was saved successfully: ') + chalk.blue.bold(fileName) + '\n');

      resolve();
    });
  });
}
