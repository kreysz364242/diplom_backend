const { Sequelize, sequelize } = require('../lib/sequlize')
const Model = Sequelize.Model
const Users = require('./UserModel')
class RefreshTokens extends Model {}
RefreshTokens.init({
    token: {
        type: Sequelize.STRING,
        allowNull: false
    },
    fingerprint: {
        type: Sequelize.STRING(1024),
        allowNull: false
    }, 
    updateAt: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, 

}, {
    timestamps: false,
    sequelize,
    modelName: 'RefreshToken'
});
Users.hasMany(RefreshTokens)
module.exports = RefreshTokens