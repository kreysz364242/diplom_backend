const router = require('express').Router();
const Places = require('../../models/PlaceModel')
const City = require('../../models/cityModel')
const Images = require('../../models/imagesModel')
const Users = require('../../models/UserModel')
const Comments = require('../../models/commentsModel')
const checkUser = require('../../middleware/checkUser')
const scrapper = require('../../scrapper/index')
const {
    Sequelize
} = require('../../lib/sequlize')
const createError = require('../../lib/createError')

const checkJwt = require('../../lib/decodeTokens')

const checkAdmin = (roles) => {
    return roles.indexOf('admin') !== -1
}

router.delete('/deletePlace', checkJwt, checkUser, async (user, req, resp, next) => {
    try {
        console.log(user.roles)
        if (checkAdmin(user.roles)) {
            const placeId = req.query.placeId
            await Places.destroy({
                where: {
                    id: placeId
                }
            })
            resp.status(200).json({})
        } else {
            return next(createError('you have no right to do this', 402))
        }
    } catch (error) {
        return next(createError('db error', 500))
    }
})

router.post('/addPlace', checkJwt, checkUser, async (user, req, resp, next) => {
    try {
        const {
            body: {
                data
            }
        } = req;
        if (checkAdmin(user.roles)) {
            const city = await City.findOne({
                where: {
                    cityName: data.cityName
                }
            })
            if (city) {
                await Places.create({
                    location: data.location,
                    locationName: data.locationName,
                    locationDescription: data.locationDescription,
                    CityId: city.id
                })

                resp.status(200).json({})
            } else {
                return next(createError('there is no such city', 402))
            }
        } else {
            return next(createError('you have no right to do this', 402))
        }
    } catch (error) {
        console.log(error)
        return next(createError('db error', 500))
    }
})

router.post('/useScrapper', checkJwt, checkUser, async (user, req, resp, next) => {
    try {
        const {
            body: {
                data
            }
        } = req;
        if (checkAdmin(user.roles)) {
            console.log(data)
            scrapper(data.url, Number(data.numberOfPages), data.city)
        } else {
            return next(createError('you have no right to do this', 402))
        }
    } catch (error) {
        console.log(error)
        return next(createError('db error', 500))
    }
})

router.patch('/changePlace', checkJwt, checkUser, async (user, req, resp, next) => {
    try {
        const {
            body: {
                data
            }
        } = req;
        console.log(user.roles)
        if (checkAdmin(user.roles)) {
            const place = await Places.findOne({
                where: {
                    id: data.id
                }
            })
            if (place) {
                await Places.update({
                    location: data.location,
                    locationName: data.locationName,
                    locationDescription: data.locationDescription
                }, {
                    where: {
                        id: data.id
                    }
                })

                resp.status(200).json({})
            } else {
                return next(createError('there is no such place', 402))
            }
        } else {
            return next(createError('you have no right to do this', 402))
        }
    } catch (error) {
        console.log(error)
        return next(createError('db error', 500))
    }
})

module.exports = router